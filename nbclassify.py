import sys
import os
import math
import json


if(len(sys.argv) != 3 ):
	print ("USAGE : python nbclassify.py modelfile testfile ----------------------------------TRY AGAIN")
	exit()

modelfile = open(sys.argv[1],"r")
full = modelfile.read()
uwords = int(full.split('\n')[0])
dictofclass = json.loads(full.split('\n')[1])

totalfiles = 0 

for x in dictofclass.values():
	totalfiles = totalfiles + x["files"]

testfile = open(sys.argv[2],"r")

for line in testfile:
	
	presentwords = {}

	for x in dictofclass.values():
		x["probs"] = math.log(x["files"]/totalfiles,10)
		#print(x["name"]+ ":" + str(x["probs"]) + "\n")

	for feature in line.split(' '):
		if feature not in presentwords:

			presentwords[feature] = 1

			feature = feature.strip(' \t\n\r')
			for x in dictofclass.values():


				if feature in x["dict"]:
					x["probs"] = x["probs"] + math.log(x["dict"][feature]/(x["words"]+uwords),10)
					#print(feature+">>"+x["name"]+":"+str(x["probs"]) + "\n")
				else:
					x["probs"] = x["probs"] + math.log(1/(x["words"]+uwords),10)
					#print(feature+">>"+x["name"]+":"+str(x["probs"]) + "\n")

	maxi = -sys.maxsize ; 

	for x in dictofclass.values():
		if(x["probs"] > maxi):
			maxi = x["probs"]
			clas = x["name"]

	print(clas)





