import sys
import os
import json

if(len(sys.argv) != 3 ):
	print ("USAGE : python nblearn.py trainingfile modelfile ----------------------------------TRY AGAIN")
	exit()

dictofclass = {}
ulist = {}

trainingfile = open(sys.argv[1],encoding='utf-8', errors='ignore')
modelfile = open(sys.argv[2],"w+")

for line in trainingfile:
	words = line.split(' ' , 1);
	
	if words[0] not in dictofclass:
		c1 = { "name" : "" , "dict" : {} , "files" : 0 ,"words":0 }
		c1["name"] = words[0]
		dictofclass[words[0]] = c1


	pw  = dictofclass[words[0]]
	pwd = pw["dict"]

	for elem in words[1].lower().split(' '):

		elem = elem.strip(' \t\n\r')
		if elem not in ulist:
			ulist[elem] = 1

		if elem in pwd:
			pwd[elem] = pwd[elem] + 1 ;
			
		else:
			pwd[elem] = 2 ;

		pw["words"] = pw["words"] + 1 ;

	pw["files"] = pw["files"] + 1;	

modelfile.write(str(len(ulist))+'\n')
modelfile.write(json.dumps(dictofclass)+'\n')
trainingfile.close()
modelfile.close()
