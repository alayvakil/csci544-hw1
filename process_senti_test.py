import sys
import os
import re

if(len(sys.argv) != 3 ):
	print ("USAGE : python3 convert1.py trainingfile_directory output_file ----------------------------------TRY AGAIN")
	exit()

trainingfile = open(sys.argv[2],"w+")
resultfile = open("gold.txt","w+")

for file in os.listdir(sys.argv[1]):
	fp = open(sys.argv[1]+"/"+file,encoding='utf-8', errors='ignore')
	s = ""
	resultfile.write(file.split('.')[0] + "\n")
	
	for line in fp:

		for word in line.split(' '):
			
			word = word.strip(' \t\n\r')

			word = re.sub(r'[^a-zA-Z0-9\-\_\'\$\@]','', word)
			if(len(word) > 0) :
				s = s + word.lower() + " "


	trainingfile.write( s+"\n")

trainingfile.close()