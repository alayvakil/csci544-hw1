How to run text classification

If you are running for spam x = spam else for sentiment x = sentiment

Suppose all the test files are in a folder test/ following the naming convention (TAG/TEST).####.txt

> python3 process_x_test.py test/ test_inputfile

This will also produce a gold.txt file where the correct tags for the test data is present.

> python3 nbclassify.py x.nb test_inputfile > x.out

This will sent class tags to the x.out file


___________________________________________________________________________________________________________________________________

----------------------------------------------------PART III-----------------------------------------------------------------------
___________________________________________________________________________________________________________________________________

Part 1 :

	Spam :
			F-score : 0.970
			Precision : 0.959
			Recall: 0.980

	Ham :

			F-score : 0.988
			Precision : 0.993
			Recall: 0.985

	Pos :	
			F-score : 0.813
			Precision : 0.736
			Recall: 0.909

	Neg :

			F-score : 0.763
			Precision : 0.880
			Recall: 0.674

Part 2 :

	SVM_Light

		Spam :
				F-score : 0.969
				Precision : 0.972
				Recall: 0.967

		Ham :

				F-score : 0.988
				Precision :0.989
				Recall:0.988

		Pos :	
				F-score :0.877
				Precision :0.876
				Recall:0.877

		Neg :

				F-score :0.877
				Precision :0.876
				Recall:0.878

	MegaM

		Spam :
				F-score : 0.58
				Precision : 0.65
				Recall: 0.53

		Ham :

				F-score : 0.82
				Precision :0.86
				Recall:0.78

		Pos :	
				F-score :0.79
				Precision :0.0.81
				Recall:0.77

		Neg :

				F-score :0.66
				Precision :0.80
				Recall:0.73


If we reduce the training data to only 10% precision recall and fscores go down drastically in all the cases as the dev tests are many and more varied than seen by the classifier while running



