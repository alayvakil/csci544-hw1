import sys
import os
import re

if(len(sys.argv) != 3 ):
	print ("USAGE : python3 convert1.py testfile_directory output_file ----------------------------------TRY AGAIN")
	exit()

trainingfile = open(sys.argv[2],"w+")
resultfile = open("gold.txt","w+")

for file in sorted(os.listdir(sys.argv[1])):
	fp = open(sys.argv[1]+"/"+file,encoding='utf-8', errors='ignore')
	s = ""
	resultfile.write(file.split('.')[0] + "\n")
	
	for line in fp:
		line = line.lower()

		for word in line.split(' '):
			word = re.sub(r'[^a-zA-Z0-9\-\_\'\$\@]','', word)
			if(len(word) > 0):
				s = s + word.strip() + " " 

			#s = s + word.strip() + " "

	trainingfile.write( s+"\n")
	print(file+ "\n")
trainingfile.close()
